import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/professor',
    name: 'Professor',
    component: () => import('../views/Officer/Professor.vue')
  },
  {
    path: '/student',
    name: 'Student',
    component: () => import('../views/Officer/Student.vue')
  },
  {
    path: '/institution',
    name: 'Institution',
    component: () => import('../views/Officer/Institution.vue')
  },
  {
    path: '/addprofessor',
    name: 'AddProfessor',
    component: () => import('../views/Officer/AddProfessor.vue')
  },
  {
    path: '/announce',
    name: 'Announce',
    component: () => import('../views/Officer/Announce.vue')
  },
  {
    path: '/training',
    name: 'IntershipTraining',
    component: () => import('../views/Officer/IntershipTraining.vue')
  },
  {
    path: '/form',
    name: 'Form',
    component: () => import('../views/Form.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/status',
    name: 'InternshipStatus',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Student/Internshipstatus.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
